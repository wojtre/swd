package client.detailedresultwindow;

import client.mainWindow.StepResult;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Wojciech Trefon on 23.05.2016.
 */
public class DetailedResultWindowController implements Initializable {

    private DetailedResultWindowModel model;
    @FXML
    private TableView<StepResult> stepResultsTable;
    @FXML
    private TableColumn<StepResult, Number> yearColumn;
    @FXML
    private TableColumn<StepResult, Number> smallMarketsToBuildColumn;
    @FXML
    private TableColumn<StepResult, Number> mediumMarketsToBuildColumn;
    @FXML
    private TableColumn<StepResult, Number> bigMarketsToBuildColumn;
    @FXML
    private TableColumn<StepResult, Number> criterionValueColumn;
    @FXML
    private TableColumn<StepResult, Number> minBudgetColumn;
    @FXML
    private TableColumn<StepResult, Number> companyValueColumn;
    @FXML
    private TableColumn<StepResult, Number> yearIncomeColumn;
    @FXML
    private TableColumn<StepResult, Number> yearCostColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        model = new DetailedResultWindowModel();
        // tutaj wywolanie metody inicjalizujacej tabele
        initializeDetailedResultTable();
    }

    public void setDetailedResult(List<StepResult> detailedResult) {
        model.getStepResults().addAll(detailedResult);
    }

    private void initializeDetailedResultTable() {
        yearColumn.setCellValueFactory(cellData -> cellData.getValue().yearProperty());
        yearColumn.setCellFactory(col -> getCellFormatterInteger());
        yearColumn.setSortType(TableColumn.SortType.ASCENDING);

        smallMarketsToBuildColumn.setCellValueFactory(cellData -> cellData.getValue().smallMarketsToBuildProperty());
        smallMarketsToBuildColumn.setCellFactory(col -> getCellFormatterInteger());

        mediumMarketsToBuildColumn.setCellValueFactory(cellData -> cellData.getValue().mediumMarketsToBuildProperty());
        mediumMarketsToBuildColumn.setCellFactory(col -> getCellFormatterInteger());

        bigMarketsToBuildColumn.setCellValueFactory(cellData -> cellData.getValue().bigMarketsToBuildProperty());
        bigMarketsToBuildColumn.setCellFactory(col -> getCellFormatterInteger());

        criterionValueColumn.setCellValueFactory(cellData -> cellData.getValue().criterionValueProperty());
        criterionValueColumn.setCellFactory(col -> getCellFormatterDouble());

        minBudgetColumn.setCellValueFactory(cellData -> cellData.getValue().minBudgetProperty());
        minBudgetColumn.setCellFactory(col -> getCellFormatterCurrency());

        companyValueColumn.setCellValueFactory(cellData -> cellData.getValue().companyValueProperty());
        companyValueColumn.setCellFactory(col -> getCellFormatterCurrency());

        yearIncomeColumn.setCellValueFactory(cellData -> cellData.getValue().yearIncomeProperty());
        yearIncomeColumn.setCellFactory(col -> getCellFormatterCurrency());

        yearCostColumn.setCellValueFactory(cellData -> cellData.getValue().yearCostProperty());
        yearCostColumn.setCellFactory(col -> getCellFormatterCurrency());


        stepResultsTable.setItems(model.getStepResults());
    }

    private TableCell<StepResult, Number> getCellFormatterCurrency() {
        return new TableCell<StepResult, Number>() {
            @Override
            public void updateItem(Number value, boolean empty) {
                super.updateItem(value, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(String.format("%.2f zł", value.doubleValue()));
                }
            }
        };
    }

    private TableCell<StepResult, Number> getCellFormatterDouble() {
        return new TableCell<StepResult, Number>() {
            @Override
            public void updateItem(Number value, boolean empty) {
                super.updateItem(value, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(String.format("%.2f", value.doubleValue()));
                }
            }
        };
    }

    private TableCell<StepResult, Number> getCellFormatterInteger() {
        return new TableCell<StepResult, Number>() {
            @Override
            public void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText("");
                } else {
                    setText(String.format("%d", item.intValue()));
                }
            }
        };
    }
}
