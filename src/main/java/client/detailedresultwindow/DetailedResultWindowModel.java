package client.detailedresultwindow;

import client.mainWindow.StepResult;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Wojciech Trefon on 23.05.2016.
 */
public class DetailedResultWindowModel {
    private ObservableList<StepResult> stepResults = FXCollections.observableArrayList();

    public ObservableList<StepResult> getStepResults() {
        return stepResults;
    }
}
