package client.common;

import javafx.scene.control.Alert;

import static javafx.scene.control.Alert.AlertType.WARNING;

/**
 * Created by Wojciech Trefon on 21.05.2016.
 */
public class IncorrectValues {
    public static void showIncorectDialog() {
        Alert alert = new Alert(WARNING);
        alert.setTitle("Błąd");
        alert.setHeaderText("Niepoprawne dane wejściowe");
        alert.setContentText("Popraw dane wejściowe");
        alert.showAndWait();
    }
}
