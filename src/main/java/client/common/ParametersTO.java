package client.common;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Wojciech Trefon on 21.05.2016.
 */
@XmlRootElement
public class ParametersTO {
    private double smallPrice;
    private double smallIncome;
    private double smallCost;

    private double mediumPrice;
    private double mediumIncome;
    private double mediumCost;

    private double bigPrice;
    private double bigIncome;
    private double bigCost;

    public double getSmallPrice() {
	return smallPrice;
    }

    public void setSmallPrice(double smallPrice) {
	this.smallPrice = smallPrice;
    }

    public double getSmallIncome() {
	return smallIncome;
    }

    public void setSmallIncome(double smallIncome) {
	this.smallIncome = smallIncome;
    }

    public double getSmallCost() {
	return smallCost;
    }

    public void setSmallCost(double smallCost) {
	this.smallCost = smallCost;
    }

    public double getMediumPrice() {
	return mediumPrice;
    }

    public void setMediumPrice(double mediumPrice) {
	this.mediumPrice = mediumPrice;
    }

    public double getMediumIncome() {
	return mediumIncome;
    }

    public void setMediumIncome(double mediumIncome) {
	this.mediumIncome = mediumIncome;
    }

    public double getMediumCost() {
	return mediumCost;
    }

    public void setMediumCost(double mediumCost) {
	this.mediumCost = mediumCost;
    }

    public double getBigPrice() {
	return bigPrice;
    }

    public void setBigPrice(double bigPrice) {
	this.bigPrice = bigPrice;
    }

    public double getBigIncome() {
	return bigIncome;
    }

    public void setBigIncome(double bigIncome) {
	this.bigIncome = bigIncome;
    }

    public double getBigCost() {
	return bigCost;
    }

    public void setBigCost(double bigCost) {
	this.bigCost = bigCost;
    }

    public double[] getPriceInArray() {
	double[] prices = new double[3];
	prices[0] = smallPrice;
	prices[1] = mediumPrice;
	prices[2] = bigPrice;
	return prices;
    }

    public double[] getIncomeInArray() {
	double[] incomes = new double[3];
	incomes[0] = smallIncome;
	incomes[1] = mediumIncome;
	incomes[2] = bigIncome;
	return incomes;
    }

    public double[] getCostInArray() {
	double[] costs = new double[3];
	costs[0] = smallCost;
	costs[1] = mediumCost;
	costs[2] = bigCost;
	return costs;
    }
}
