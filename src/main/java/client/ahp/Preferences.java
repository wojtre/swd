package client.ahp;

import javafx.util.Pair;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Created by Wojciech Trefon on 15.05.2016.
 */

@XmlRootElement
public class Preferences {
    private Pair<Integer, Integer> dochodToKoszty = new Pair<Integer, Integer>(1, 1);
    private Pair<Integer, Integer> dochodToWartosc = new Pair<Integer, Integer>(1, 1);
    private Pair<Integer, Integer> kosztyToWartosc = new Pair<Integer, Integer>(1, 1);

    public Pair<Integer, Integer> getDochodToKoszty() {
        return dochodToKoszty;
    }

    @XmlJavaTypeAdapter(PairAdapter.class)
    public void setDochodToKoszty(Pair<Integer, Integer> dochodToKoszty) {
        this.dochodToKoszty = dochodToKoszty;
    }

    public Pair<Integer, Integer> getDochodToWartosc() {
        return dochodToWartosc;
    }

    @XmlJavaTypeAdapter(PairAdapter.class)
    public void setDochodToWartosc(Pair<Integer, Integer> dochodToWartosc) {
        this.dochodToWartosc = dochodToWartosc;
    }

    public Pair<Integer, Integer> getKosztyToWartosc() {
        return kosztyToWartosc;
    }

    @XmlJavaTypeAdapter(PairAdapter.class)
    public void setKosztyToWartosc(Pair<Integer, Integer> kosztyToWartosc) {
        this.kosztyToWartosc = kosztyToWartosc;
    }

    private static class PairElements {
        @XmlElement
        public Integer numerator;
        @XmlElement
        public Integer denominator;

        private PairElements() {
        }

        public PairElements(Integer numerator, Integer denominator) {
            this.numerator = numerator;
            this.denominator = denominator;
        }
    }

    private static class PairAdapter extends XmlAdapter<PairElements, Pair<Integer, Integer>> {
        public PairElements marshal(Pair<Integer, Integer> arg0) throws Exception {
            return new PairElements(arg0.getKey(), arg0.getValue());
        }

        public Pair<Integer, Integer> unmarshal(PairElements arg0) throws Exception {
            return new Pair<Integer, Integer>(arg0.numerator, arg0.denominator);
        }
    }

    public double getDochodToKosztyProportion() {
        return ((double) dochodToKoszty.getKey()) / dochodToKoszty.getValue();
    }

    public double getKosztyToDochodProportion() {
        return ((double) dochodToKoszty.getValue()) / dochodToKoszty.getKey();
    }

    public double getDochodToWartoscProportion() {
        return ((double) dochodToWartosc.getKey()) / dochodToWartosc.getValue();
    }

    public double getWartoscToDochodProportion() {
        return ((double) dochodToWartosc.getValue()) / dochodToWartosc.getKey();
    }

    public double getKosztyToWartoscProportion() {
        return ((double) kosztyToWartosc.getKey()) / kosztyToWartosc.getValue();
    }

    public double getWartoscToKosztyProportion() {
        return ((double) kosztyToWartosc.getValue()) / kosztyToWartosc.getKey();
    }
}
