package client.ahp;

/**
 * Created by Wojciech Trefon on 15.05.2016.
 */
public class AhpModel {
    private Preferences ahpPreferences = new Preferences();
    // kolejnosc: dochod, koszty, wartosc
    private double[] ahpProportion = {0.33, 0.33, 0.33};

    public Preferences getAhpPreferences() {
        return ahpPreferences;
    }

    public void setAhpPreferences(Preferences ahpPreferences) {
        this.ahpPreferences = ahpPreferences;
    }

    public double[] getAhpProportion() {
        return ahpProportion;
    }

    public void setAhpProportion(double[] ahpProportion) {
        this.ahpProportion = ahpProportion;
    }
}
