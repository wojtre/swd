package client.ahp;

import static client.ahp.IdText.*;

/**
 * Created by Wojciech Trefon on 02.06.2016.
 */
public class PreferencesConverter {
    public static String convertFromInteger(Integer i) {
        switch (i){
            case 1 : return PREF_1;
            case 3 : return PREF_3;
            case 5 : return PREF_5;
            case 7 : return PREF_7;
            case 9 : return PREF_9;
            default: return PREF_1;
        }
    }

    public static Integer convertFromString(String s) {
        switch (s) {
            case PREF_1: return 1;
            case PREF_3: return 3;
            case PREF_5: return 5;
            case PREF_7: return 7;
            case PREF_9: return 9;
            default: return 1;
        }
    }
}
