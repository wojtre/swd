package client.ahp;

/**
 * Created by Wojciech Trefon on 15.05.2016.
 */
public interface IdText {
    String DOCHOD = "parametr dochód";
    String KOSZTY = "parametr koszty";
    String WARTOSC_FIRMY = "parametr wartość firmy";
    String PREF_1 = "jest tak samo ważny jak";
    String PREF_3 = "jest trochę ważniejszy niż";
    String PREF_5 = "jest ważniejszy niż";
    String PREF_7 = "jest wyraźnie ważniejszy niż";
    String PREF_9 = "jest o wiele ważniejszy niż";
}
