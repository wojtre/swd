package client.ahp;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Pair;
import logic.AlgorithmsFacade;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import static client.ahp.IdText.*;
import static client.ahp.PreferencesConverter.convertFromInteger;
import static client.ahp.PreferencesConverter.convertFromString;
import static javafx.scene.control.Alert.AlertType.CONFIRMATION;
import static javafx.scene.control.Alert.AlertType.WARNING;

/**
 * Created by Wojciech Trefon on 14.05.2016.
 */
public class AhpController implements Initializable {

    private AhpModel model;

    private AlgorithmsFacade facade;

    @FXML
    private Label row1Up;

    @FXML
    private Label row1Down;

    @FXML
    private Label row2Up;

    @FXML
    private Label row2Down;

    @FXML
    private Label row3Up;

    @FXML
    private Label row3Down;

    @FXML
    private ComboBox<String> dochodToKosztyCb;

    @FXML
    private ComboBox<String> dochodToWartoscCb;

    @FXML
    private ComboBox<String> kosztyToWartoscCb;

    public Preferences getPreferences() {
	return model.getAhpPreferences();
    }

    public double[] getAhpProportions() {
	return model.getAhpProportion();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
	model = new AhpModel();
	facade = new AlgorithmsFacade();
	setUpDefaultLabelsText();
	restoreDefault();
	refreshView();
    }

    @FXML
    private void save(ActionEvent e) {
	if (facade.isMatrixConsistent(model.getAhpPreferences())) {
	    double[] proportions = facade.calculateAhpProportions(model.getAhpPreferences());
	    model.setAhpProportion(proportions);
	    closeWindow(e);
	} else {
	    if (determinateIfUserWantImprovement()) {
		Preferences improved = facade.improveMatrixConsistency(getPreferences());
		if (improved != null) {
		    model.setAhpPreferences(improved);
		    refreshView();
		} else {
		    Alert alert = new Alert(WARNING);
		    alert.setTitle("Niespójność preferencji");
		    alert.setHeaderText("Nie udało poprawić się spójności preferencji");
		    alert.setContentText("Sprobój poprawić je ręcznie");
		    alert.showAndWait();
		}
	    }
	}

    }

    private void closeWindow(ActionEvent e) {
	Button saveButton = (Button) e.getSource();
	Stage window = (Stage) saveButton.getScene().getWindow();
	window.close();
    }

    private boolean determinateIfUserWantImprovement() {
	Alert alert = new Alert(CONFIRMATION);
	alert.setTitle("Niespójność preferencji");
	alert.setHeaderText("Wprowadzone preferencje są niespójne");
	alert.setContentText("Czy chcesz, aby zostały skorygowane automatycznie?");

	Optional<ButtonType> result = alert.showAndWait();
	return result.get() == ButtonType.OK;
    }

    @FXML
    private void toggle1() {
	swapTexts(row1Up, row1Down);
	calculatePreferences();
    }

    @FXML
    private void toggle2() {
	swapTexts(row2Up, row2Down);
	calculatePreferences();
    }

    @FXML
    private void toggle3() {
	swapTexts(row3Up, row3Down);
	calculatePreferences();
    }

    @FXML
    private void changedPrefValue() {
	calculatePreferences();
    }

    @FXML
    private void saveAsDefault() {
	try {

	    String canonicalPath = new File(".").getCanonicalPath();
	    String replace = canonicalPath.replace("\\", "\\\\");
	    File file = new File(replace + "\\src\\main\\resources\\preferences.xml");
	    file.createNewFile();
	    JAXBContext jaxbContext = JAXBContext.newInstance(Preferences.class);
	    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

	    // output pretty printed
	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	    jaxbMarshaller.marshal(model.getAhpPreferences(), file);
	    jaxbMarshaller.marshal(model.getAhpPreferences(), System.out);

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    @FXML
    private void restoreDefault() {
	try {
	    String canonicalPath = new File(".").getCanonicalPath();
	    String replace = canonicalPath.replace("\\", "\\\\");
	    File file = new File(replace + "\\src\\main\\resources\\preferences.xml");

	    JAXBContext jaxbContext = JAXBContext.newInstance(Preferences.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    Preferences preferences = (Preferences) jaxbUnmarshaller.unmarshal(file);
	    model.setAhpPreferences(preferences);
	    refreshView();

	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    private void setUpDefaultLabelsText() {
	row1Up.setText(DOCHOD);
	row1Down.setText(KOSZTY);
	row2Up.setText(DOCHOD);
	row2Down.setText(WARTOSC_FIRMY);
	row3Up.setText(KOSZTY);
	row3Down.setText(WARTOSC_FIRMY);
    }

    private void swapTexts(Label lb1, Label lb2) {
	String text = lb1.getText();
	lb1.setText(lb2.getText());
	lb2.setText(text);
    }

    private void calculatePreferences() {
	Preferences preferences = new Preferences();

	if (DOCHOD.equals(row1Up.getText())) {
	    preferences.setDochodToKoszty(new Pair<Integer, Integer>(convertFromString(dochodToKosztyCb.getValue()), 1));
	} else {
	    preferences.setDochodToKoszty(new Pair<Integer, Integer>(1, convertFromString(dochodToKosztyCb.getValue())));
	}
	if (DOCHOD.equals(row2Up.getText())) {
	    preferences.setDochodToWartosc(new Pair<Integer, Integer>(convertFromString(dochodToWartoscCb.getValue()), 1));
	} else {
	    preferences.setDochodToWartosc(new Pair<Integer, Integer>(1, convertFromString(dochodToWartoscCb.getValue())));
	}
	if (KOSZTY.equals(row3Up.getText())) {
	    preferences.setKosztyToWartosc(new Pair<Integer, Integer>(convertFromString(kosztyToWartoscCb.getValue()), 1));

	} else {
	    preferences.setKosztyToWartosc(new Pair<Integer, Integer>(1, convertFromString(kosztyToWartoscCb.getValue())));
	}
	model.setAhpPreferences(preferences);
    }

    private void refreshView() {
	Preferences ahpPreferences = model.getAhpPreferences();
	resolveDochodToKoszty(ahpPreferences);
	resolveDochodToWartosc(ahpPreferences);
	resolveKosztyToWartosc(ahpPreferences);
    }

    private void resolveKosztyToWartosc(Preferences ahpPreferences) {
	Pair<Integer, Integer> kosztyToWartosc = ahpPreferences.getKosztyToWartosc();
	if (kosztyToWartosc.getKey().equals(kosztyToWartosc.getValue())) {
	    kosztyToWartoscCb.setValue(convertFromInteger(kosztyToWartosc.getKey()));
	    return;
	}
	row3Up.setText(KOSZTY);
	row3Down.setText(WARTOSC_FIRMY);
	if (!new Integer(1).equals(kosztyToWartosc.getKey())) {
	    kosztyToWartoscCb.setValue(convertFromInteger(kosztyToWartosc.getKey()));
	} else {
	    kosztyToWartoscCb.setValue(convertFromInteger(kosztyToWartosc.getValue()));
	    swapTexts(row3Up, row3Down);
	}
    }

    private void resolveDochodToWartosc(Preferences ahpPreferences) {
	Pair<Integer, Integer> dochodToWartosc = ahpPreferences.getDochodToWartosc();
	if (dochodToWartosc.getKey().equals(dochodToWartosc.getValue())) {
	    dochodToWartoscCb.setValue(convertFromInteger(dochodToWartosc.getKey()));
	    return;
	}
	row2Up.setText(DOCHOD);
	row2Down.setText(WARTOSC_FIRMY);
	if (!new Integer(1).equals(dochodToWartosc.getKey())) {
	    dochodToWartoscCb.setValue(convertFromInteger(dochodToWartosc.getKey()));
	} else {
	    dochodToWartoscCb.setValue(convertFromInteger(dochodToWartosc.getValue()));
	    swapTexts(row2Up, row2Down);
	}
    }

    private void resolveDochodToKoszty(Preferences ahpPreferences) {
	Pair<Integer, Integer> dochodToKoszty = ahpPreferences.getDochodToKoszty();
	if (dochodToKoszty.getKey().equals(dochodToKoszty.getValue())) {
	    dochodToKosztyCb.setValue(convertFromInteger(dochodToKoszty.getKey()));
	    return;
	}
	row1Up.setText(DOCHOD);
	row1Down.setText(KOSZTY);
	if (!new Integer(1).equals(dochodToKoszty.getKey())) {
	    dochodToKosztyCb.setValue(convertFromInteger(dochodToKoszty.getKey()));
	} else {
	    dochodToKosztyCb.setValue(convertFromInteger(dochodToKoszty.getValue()));
	    swapTexts(row1Up, row1Down);
	}
    }
}
