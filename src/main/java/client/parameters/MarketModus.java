package client.parameters;

/**
 * Created by Wojciech Trefon on 21.05.2016.
 */
public enum MarketModus {
    SMALL,
    MEDIUM,
    BIG;

    public MarketModus getNextModus() {
        if (this == SMALL) {
            return MEDIUM;
        } else if (this == MEDIUM) {
            return BIG;
        }
        throw new UnsupportedOperationException();
    }

    public MarketModus getPreviousModus() {
        if (this == BIG) {
            return MEDIUM;
        } else if (this == MEDIUM) {
            return SMALL;
        }
        throw new UnsupportedOperationException();
    }
}
