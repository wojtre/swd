package client.parameters;

import client.common.ParametersTO;

import static client.parameters.MarketModus.SMALL;

/**
 * Created by Wojciech Trefon on 21.05.2016.
 */
public class ParametersModel {
    private ParametersTO parametersTO = new ParametersTO();
    private MarketModus modus = SMALL;

    public ParametersTO getParametersTO() {
        return parametersTO;
    }

    public void setParametersTO(ParametersTO parametersTO) {
        this.parametersTO = parametersTO;
    }

    public MarketModus getModus() {
        return modus;
    }

    public void setModus(MarketModus modus) {
        this.modus = modus;
    }
}
