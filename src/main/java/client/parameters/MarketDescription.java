package client.parameters;

/**
 * Created by Wojciech Trefon on 21.05.2016.
 */
public interface MarketDescription {
    String SMALL_MARKET_DESCR = "małej Stonki";
    String MEDIUM_MARKET_DESCR = "średniej Stonki";
    String BIG_MARKET_DESCR = "dużej Stonki";
}
