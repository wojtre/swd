package client.parameters;

import static client.parameters.MarketDescription.BIG_MARKET_DESCR;
import static client.parameters.MarketDescription.MEDIUM_MARKET_DESCR;
import static client.parameters.MarketDescription.SMALL_MARKET_DESCR;
import static client.parameters.MarketModus.BIG;
import static client.parameters.MarketModus.MEDIUM;
import static client.parameters.MarketModus.SMALL;
import static java.lang.Double.parseDouble;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import client.common.IncorrectValues;
import client.common.ParametersTO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Created by Wojciech Trefon on 21.05.2016.
 */
public class ParametersController implements Initializable {
    private ParametersModel model;

    @FXML
    private Label marketType;

    @FXML
    private TextField priceSmall;

    @FXML
    private TextField costSmall;

    @FXML
    private TextField incomeSmall;

    @FXML
    private TextField priceMedium;

    @FXML
    private TextField costMedium;

    @FXML
    private TextField incomeMedium;

    @FXML
    private TextField priceBig;

    @FXML
    private TextField costBig;

    @FXML
    private TextField incomeBig;

    @FXML
    private Button next;

    @FXML
    private Button previous;

    @FXML
    private Button save;

    @FXML
    private Button saveAsDefault;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

	model = new ParametersModel();
	try {
	    String canonicalPath = new File(".").getCanonicalPath();
	    String replace = canonicalPath.replace("\\", "\\\\");
	    File file = new File(replace + "\\src\\main\\resources\\parameters.xml");
	    JAXBContext jaxbContext = JAXBContext.newInstance(ParametersTO.class);

	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    ParametersTO parametersTO = (ParametersTO) jaxbUnmarshaller.unmarshal(file);
	    model.setParametersTO(parametersTO);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	setTestFieldsValuesFromModel();
	resolveElementsVisibility(model.getModus()); // <- to musi byc na koncu
    }

    public ParametersTO getParametersTO() {
	return model.getParametersTO();
    }

    @FXML
    private void save(ActionEvent e) {
	try {
	    saveDataFromViewDependOnModus();
	    closeWindow(e);
	} catch (Exception ex) {
	    IncorrectValues.showIncorectDialog();
	}
    }

    @FXML
    private void saveAsDefault() {

	try {
	    saveDataFromViewDependOnModus();
	    String canonicalPath = new File(".").getCanonicalPath();
	    String replace = canonicalPath.replace("\\", "\\\\");
	    File file = new File(replace + "\\src\\main\\resources\\parameters.xml");
	    JAXBContext jaxbContext = JAXBContext.newInstance(ParametersTO.class);
	    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

	    // output pretty printed
	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	    jaxbMarshaller.marshal(model.getParametersTO(), file);
	    jaxbMarshaller.marshal(model.getParametersTO(), System.out);
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    @FXML
    private void restoreDefault() {
	// po wczytaniu wartosci do modelu musimy zasetowac je w textFieldach
	setTestFieldsValuesFromModel();
    }

    @FXML
    private void next() {
	try {
	    saveDataFromViewDependOnModus();
	    MarketModus nextModus = model.getModus().getNextModus();
	    resolveElementsVisibility(nextModus);
	    model.setModus(nextModus);
	} catch (Exception e) {
	    IncorrectValues.showIncorectDialog();
	}
    }

    @FXML
    private void previous() {
	MarketModus previousModus = model.getModus().getPreviousModus();
	resolveElementsVisibility(previousModus);
	model.setModus(previousModus);
    }

    private void closeWindow(ActionEvent e) {
	Button saveButton = (Button) e.getSource();
	Stage window = (Stage) saveButton.getScene().getWindow();
	window.close();
    }

    private void resolveElementsVisibility(MarketModus modus) {
	if (modus == SMALL) {
	    setForSmall();
	} else if (modus == MEDIUM) {
	    setForMedium();
	} else if (modus == BIG) {
	    setForBig();
	}
    }

    private void setTestFieldsValuesFromModel() {
	costSmall.setText(String.valueOf(model.getParametersTO().getSmallCost()));
	priceSmall.setText(String.valueOf(model.getParametersTO().getSmallPrice()));
	incomeSmall.setText(String.valueOf(model.getParametersTO().getSmallIncome()));

	costMedium.setText(String.valueOf(model.getParametersTO().getMediumCost()));
	priceMedium.setText(String.valueOf(model.getParametersTO().getMediumPrice()));
	incomeMedium.setText(String.valueOf(model.getParametersTO().getMediumIncome()));

	costBig.setText(String.valueOf(model.getParametersTO().getBigCost()));
	priceBig.setText(String.valueOf(model.getParametersTO().getBigPrice()));
	incomeBig.setText(String.valueOf(model.getParametersTO().getBigIncome()));
    }

    private void setForBig() {
	previous.setVisible(true);
	next.setVisible(false);
	marketType.setText(BIG_MARKET_DESCR);
	save.setVisible(true);
	saveAsDefault.setVisible(true);

	priceMedium.setVisible(false);
	incomeMedium.setVisible(false);
	costMedium.setVisible(false);

	priceBig.setVisible(true);
	incomeBig.setVisible(true);
	costBig.setVisible(true);

	priceSmall.setVisible(false);
	incomeSmall.setVisible(false);
	costSmall.setVisible(false);
    }

    private void setForMedium() {
	previous.setVisible(true);
	next.setVisible(true);
	marketType.setText(MEDIUM_MARKET_DESCR);
	save.setVisible(false);
	saveAsDefault.setVisible(false);

	priceMedium.setVisible(true);
	incomeMedium.setVisible(true);
	costMedium.setVisible(true);

	priceBig.setVisible(false);
	incomeBig.setVisible(false);
	costBig.setVisible(false);

	priceSmall.setVisible(false);
	incomeSmall.setVisible(false);
	costSmall.setVisible(false);
    }

    private void setForSmall() {
	previous.setVisible(false);
	marketType.setText(SMALL_MARKET_DESCR);
	next.setVisible(true);
	save.setVisible(false);
	saveAsDefault.setVisible(false);

	priceMedium.setVisible(false);
	incomeMedium.setVisible(false);
	costMedium.setVisible(false);

	priceBig.setVisible(false);
	incomeBig.setVisible(false);
	costBig.setVisible(false);

	priceSmall.setVisible(true);
	incomeSmall.setVisible(true);
	costSmall.setVisible(true);
    }

    private void saveDataFromViewDependOnModus() throws Exception {
	MarketModus currentModus = model.getModus();
	if (currentModus == SMALL) {
	    model.getParametersTO().setSmallPrice(parseDouble(priceSmall.getText()));
	    model.getParametersTO().setSmallCost(parseDouble(costSmall.getText()));
	    model.getParametersTO().setSmallIncome(parseDouble(incomeSmall.getText()));
	    if (isAnyZeroOrNegative(model.getParametersTO().getSmallCost(), model.getParametersTO().getSmallIncome(),
		    model.getParametersTO().getSmallPrice())) {
		throw new ArithmeticException();
	    }
	} else if (currentModus == MEDIUM) {
	    model.getParametersTO().setMediumPrice(parseDouble(priceMedium.getText()));
	    model.getParametersTO().setMediumCost(parseDouble(costMedium.getText()));
	    model.getParametersTO().setMediumIncome(parseDouble(incomeMedium.getText()));
	    if (isAnyZeroOrNegative(model.getParametersTO().getMediumCost(), model.getParametersTO().getMediumIncome(),
		    model.getParametersTO().getMediumPrice())) {
		throw new ArithmeticException();
	    }
	} else if (currentModus == BIG) {
	    model.getParametersTO().setBigPrice(parseDouble(priceBig.getText()));
	    model.getParametersTO().setBigCost(parseDouble(costBig.getText()));
	    model.getParametersTO().setBigIncome(parseDouble(incomeBig.getText()));
	    if (isAnyZeroOrNegative(model.getParametersTO().getBigCost(), model.getParametersTO().getBigIncome(),
		    model.getParametersTO().getBigPrice())) {
		throw new ArithmeticException();
	    }
	}
    }

    private boolean isAnyZeroOrNegative(double... values) {
	for (double v : values) {
	    if (v <= 0) {
		return true;
	    }
	}
	return false;
    }
}
