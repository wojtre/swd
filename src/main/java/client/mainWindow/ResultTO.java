package client.mainWindow;

import java.util.List;

/**
 * Created by Wojciech Trefon on 20.05.2016.
 */
public class ResultTO {
    private List<GeneralResult> generalResults;

    public List<GeneralResult> getGeneralResults() {
        return generalResults;
    }

    public void setGeneralResults(List<GeneralResult> generalResults) {
        this.generalResults = generalResults;
    }
}
