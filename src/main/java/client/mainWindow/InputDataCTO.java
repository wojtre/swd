package client.mainWindow;

import client.common.ParametersTO;

/**
 * Created by Wojciech Trefon on 17.05.2016.
 */
public class InputDataCTO {
    private ParametersTO parametersTO = new ParametersTO();
    private int yearsNr;
    private double budget = 0;
    private int smallNr;
    private int mediumNr;
    private int bigNr;
    private double dochodProportion = 0.33;
    private double kosztyProportion = 0.33;
    private double wartoscProportion = 0.33;
    private int marketsPerYear = 1;

    public ParametersTO getParametersTO() {
        return parametersTO;
    }

    public void setParametersTO(ParametersTO parametersTO) {
        this.parametersTO = parametersTO;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public int getSmallNr() {
        return smallNr;
    }

    public void setSmallNr(int smallNr) {
        this.smallNr = smallNr;
    }

    public int getMediumNr() {
        return mediumNr;
    }

    public void setMediumNr(int mediumNr) {
        this.mediumNr = mediumNr;
    }

    public int getBigNr() {
        return bigNr;
    }

    public void setBigNr(int bigNr) {
        this.bigNr = bigNr;
    }

    public int getYearsNr() {
        return yearsNr;
    }

    public void setYearsNr(int yearsNr) {
        this.yearsNr = yearsNr;
    }

    public double getDochodProportion() {
        return dochodProportion;
    }

    public void setDochodProportion(double dochodProportion) {
        this.dochodProportion = dochodProportion;
    }

    public double getKosztyProportion() {
        return kosztyProportion;
    }

    public void setKosztyProportion(double kosztyProportion) {
        this.kosztyProportion = kosztyProportion;
    }

    public double getWartoscProportion() {
        return wartoscProportion;
    }

    public void setWartoscProportion(double wartoscProportion) {
        this.wartoscProportion = wartoscProportion;
    }

    public int getMarketsPerYear() {
        return marketsPerYear;
    }

    public void setMarketsPerYear(int marketsPerYear) {
        this.marketsPerYear = marketsPerYear;
    }
}
