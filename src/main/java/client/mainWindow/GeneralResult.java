package client.mainWindow;

import javafx.beans.property.SimpleDoubleProperty;

import java.util.List;

/**
 * Created by Wojciech Trefon on 20.05.2016.
 */
public class GeneralResult {
    private SimpleDoubleProperty criterionValue;
    private SimpleDoubleProperty minBudget;
    private SimpleDoubleProperty companyValue;
    private SimpleDoubleProperty yearIncome;
    private SimpleDoubleProperty yearCost;
    private List<StepResult> detailedResult;

    public GeneralResult(Double criterionValue, Double minBudget, Double companyValue, Double yearIncomeValue, Double yearCost) {
        this.criterionValue = new SimpleDoubleProperty(criterionValue);
        this.minBudget = new SimpleDoubleProperty(minBudget);
        this.companyValue = new SimpleDoubleProperty(companyValue);
        this.yearIncome = new SimpleDoubleProperty(yearIncomeValue);
        this.yearCost = new SimpleDoubleProperty(yearCost);
    }

    public Double getCriterionValue() {
        return criterionValue.get();
    }

    public SimpleDoubleProperty criterionValueProperty() {
        return criterionValue;
    }

    public Double getMinBudget() {
        return minBudget.get();
    }

    public SimpleDoubleProperty minBudgetProperty() {
        return minBudget;
    }

    public Double getCompanyValue() {
        return companyValue.get();
    }

    public SimpleDoubleProperty companyValueProperty() {
        return companyValue;
    }

    public Double getYearIncome() {
        return yearIncome.get();
    }

    public SimpleDoubleProperty yearIncomeProperty() {
        return yearIncome;
    }

    public Double getYearCost() {
        return yearCost.get();
    }

    public SimpleDoubleProperty yearCostProperty() {
        return yearCost;
    }

    public List<StepResult> getDetailedResult() {
        return detailedResult;
    }

    public void setDetailedResult(List<StepResult> detailedResult) {
        this.detailedResult = detailedResult;
    }
}
