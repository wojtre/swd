package client.mainWindow;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Created by Wojciech Trefon on 15.05.2016.
 */
public class MainWindowModel {
    private InputDataCTO inputDataCTO = new InputDataCTO();
    private ObservableList<GeneralResult> generalResults = FXCollections.observableArrayList();


    public InputDataCTO getInputDataCTO() {
        return inputDataCTO;
    }

    public void setInputDataCTO(InputDataCTO inputDataCTO) {
        this.inputDataCTO = inputDataCTO;
    }

    public ObservableList<GeneralResult> getGeneralResults() {
        return generalResults;
    }

    public void setGeneralResults(ObservableList<GeneralResult> generalResults) {
        this.generalResults = generalResults;
    }
}
