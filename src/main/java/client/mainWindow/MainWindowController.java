package client.mainWindow;

import client.ahp.AhpController;
import client.common.IncorrectValues;
import client.common.ParametersTO;
import client.detailedresultwindow.DetailedResultWindowController;
import client.parameters.ParametersController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logic.AlgorithmsFacade;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseUnsignedInt;

/**
 * Created by Wojciech Trefon on 14.05.2016.
 */
public class MainWindowController implements Initializable {
    private MainWindowModel model;
    private Stage ahpStage;
    private AhpController ahpController;
    private Stage parametersStage;
    private ParametersController parametersController;
    private AlgorithmsFacade facade;

    // table
    @FXML
    private TableView<GeneralResult> generalResultTable;

    @FXML
    private TableColumn<GeneralResult, Number> criterionValueColumn;

    @FXML
    private TableColumn<GeneralResult, Number> minBudgetColumn;

    @FXML
    private TableColumn<GeneralResult, Number> companyValueColumn;

    @FXML
    private TableColumn<GeneralResult, Number> yearIncomeColumn;

    @FXML
    private TableColumn<GeneralResult, Number> yearCostColumn;

    //// table

    @FXML
    private TextField yearsNr;

    @FXML
    private TextField budget;

    @FXML
    private TextField smallNr;

    @FXML
    private TextField mediumNr;

    @FXML
    private TextField bigNr;

    @FXML
    private TextField dochodProportion;

    @FXML
    private TextField kosztyProportion;

    @FXML
    private TextField wartoscProportion;

    @FXML
    private TextField marketsPerYear;

    @FXML
    private void showHelp() {
        String canonicalPath = null;
        try {
            canonicalPath = new File(".").getCanonicalPath();
            String replace = canonicalPath.replace("\\", "\\\\");
            File file = new File(replace + "\\src\\main\\resources\\documents\\instruction\\instrukcja.pdf");
            if (file != null) {
                new Thread(() -> {
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void openAhp(ActionEvent e) throws IOException {
        ahpStage.showAndWait();
        model.getInputDataCTO().setDochodProportion(ahpController.getAhpProportions()[0]);
        model.getInputDataCTO().setKosztyProportion(ahpController.getAhpProportions()[1]);
        model.getInputDataCTO().setWartoscProportion(ahpController.getAhpProportions()[2]);
        refreshAhpResult();
    }

    @FXML
    private void openParametersDialog() {
        parametersStage.showAndWait();
        model.getInputDataCTO().setParametersTO(parametersController.getParametersTO());
    }

    @FXML
    private void calculate() {
        try {
            collectInputDataFromViewAndValidate();
            ResultTO resultTO = facade.calculateResult(model.getInputDataCTO());
            refreshTable(resultTO);
        } catch (Exception e) {
            IncorrectValues.showIncorectDialog();
        }
    }

    @FXML
    private void openDetailedResult(MouseEvent click) {
        if (click.getClickCount() > 1) {
            GeneralResult selectedItem = generalResultTable.getSelectionModel().getSelectedItem();
            Pane root = null;
            Stage detailedResultStage = new Stage();
            FXMLLoader loader = new FXMLLoader(
                    getClass().getResource("/client/detailedresultwindow/detailedResultWindow.fxml"));
            try {
                root = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            detailedResultStage.setScene(new Scene(root));
            detailedResultStage.setTitle("Szczegółowe rozwiązanie");
            detailedResultStage.initModality(Modality.APPLICATION_MODAL);
            loader.<DetailedResultWindowController>getController().setDetailedResult(selectedItem.getDetailedResult());

            detailedResultStage.showAndWait();
        }
    }

    private void refreshTable(ResultTO resultTO) {
        model.getGeneralResults().clear();
        model.getGeneralResults().addAll(resultTO.getGeneralResults());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        model = new MainWindowModel();

        try {
            String canonicalPath = new File(".").getCanonicalPath();
            String replace = canonicalPath.replace("\\", "\\\\");
            File file = new File(replace + "\\src\\main\\resources\\parameters.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(ParametersTO.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            ParametersTO parametersTO = (ParametersTO) jaxbUnmarshaller.unmarshal(file);
            model.getInputDataCTO().setParametersTO(parametersTO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        initializeAhpDialog();
        initializePreferencesDialog();
        initializeGeneralResultTable();
        facade = new AlgorithmsFacade();
    }

    private void initializeAhpDialog() {
        Pane root = null;
        ahpStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/client/ahp/ahp.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ahpStage.setScene(new Scene(root));
        ahpStage.setTitle("Wybierz preferencje");
        ahpStage.initModality(Modality.APPLICATION_MODAL);
        ahpController = loader.<AhpController>getController();
    }

    private void initializePreferencesDialog() {
        Pane root = null;
        parametersStage = new Stage();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/client/parameters/ParametersWindow.fxml"));
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        parametersStage.setScene(new Scene(root));
        parametersStage.setTitle("Uzupełnij parametry Stonek");
        parametersStage.initModality(Modality.APPLICATION_MODAL);
        parametersController = loader.<ParametersController>getController();
    }

    private void refreshAhpResult() {
        dochodProportion.setText(String.valueOf(model.getInputDataCTO().getDochodProportion()).substring(0, 3));
        kosztyProportion.setText(String.valueOf(model.getInputDataCTO().getKosztyProportion()).substring(0, 3));
        wartoscProportion.setText(String.valueOf(model.getInputDataCTO().getWartoscProportion()).substring(0, 3));
    }

    private void collectInputDataFromViewAndValidate() throws Exception {
        model.getInputDataCTO().setYearsNr(parseUnsignedInt(yearsNr.getText()));
        if (!budget.getText().isEmpty()) {
            model.getInputDataCTO().setBudget(parseDouble(budget.getText()));
        } else {
            model.getInputDataCTO().setBudget(0);
        }
        model.getInputDataCTO().setSmallNr(parseUnsignedInt(smallNr.getText()));
        model.getInputDataCTO().setMediumNr(parseUnsignedInt(mediumNr.getText()));
        model.getInputDataCTO().setBigNr(parseUnsignedInt(bigNr.getText()));
        model.getInputDataCTO().setMarketsPerYear(parseUnsignedInt(marketsPerYear.getText()));
        if (isNoMarkets() || isBudgetNegative() || isNoMarketsPerYear()) {
            throw new ArithmeticException();
        }
    }

    private boolean isNoMarketsPerYear() {
        return model.getInputDataCTO().getMarketsPerYear() == 0;
    }

    private boolean isBudgetNegative() {
        return model.getInputDataCTO().getBudget() < 0;
    }

    private boolean isNoMarkets() {
        return model.getInputDataCTO().getSmallNr() == 0 && model.getInputDataCTO().getMediumNr() == 0
                && model.getInputDataCTO().getBigNr() == 0;
    }

    private void initializeGeneralResultTable() {
        criterionValueColumn.setCellValueFactory(cellData -> cellData.getValue().criterionValueProperty());
        criterionValueColumn.setCellFactory(col -> getCellFormatterDouble());
//        criterionValueColumn.setSortType(TableColumn.SortType.DESCENDING);

        minBudgetColumn.setCellValueFactory(cellData -> cellData.getValue().minBudgetProperty());
        minBudgetColumn.setCellFactory(col -> getCellFormatterCurrency());

        companyValueColumn.setCellValueFactory(cellData -> cellData.getValue().companyValueProperty());
        companyValueColumn.setCellFactory(col -> getCellFormatterCurrency());

        yearIncomeColumn.setCellValueFactory(cellData -> cellData.getValue().yearIncomeProperty());
        yearIncomeColumn.setCellFactory(col -> getCellFormatterCurrency());

        yearCostColumn.setCellValueFactory(cellData -> cellData.getValue().yearCostProperty());
        yearCostColumn.setCellFactory(col -> getCellFormatterCurrency());

        generalResultTable.setItems(model.getGeneralResults());
//        generalResultTable.getSortOrder().add(criterionValueColumn);

    }

    private TableCell<GeneralResult, Number> getCellFormatterCurrency() {
        return new TableCell<GeneralResult, Number>() {
            @Override
            public void updateItem(Number value, boolean empty) {
                super.updateItem(value, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(String.format("%.2f zł", value.doubleValue()));
                }
            }
        };
    }

    private TableCell<GeneralResult, Number> getCellFormatterDouble() {
        return new TableCell<GeneralResult, Number>() {
            @Override
            public void updateItem(Number value, boolean empty) {
                super.updateItem(value, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(String.format("%.2f", value.doubleValue()));
                }
            }
        };
    }
}
