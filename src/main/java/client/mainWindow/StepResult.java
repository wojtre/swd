package client.mainWindow;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * Created by Wojciech Trefon on 23.05.2016.
 */
public class StepResult {
    private SimpleIntegerProperty year;
    private SimpleIntegerProperty smallMarketsToBuild;
    private SimpleIntegerProperty mediumMarketsToBuild;
    private SimpleIntegerProperty bigMarketsToBuild;
    private SimpleDoubleProperty criterionValue;
    private SimpleDoubleProperty minBudget;
    private SimpleDoubleProperty companyValue;
    private SimpleDoubleProperty yearIncome;
    private SimpleDoubleProperty yearCost;


    public StepResult(Integer year, Integer smallMarketsToBuild, Integer mediumMarketsToBuild, Integer bigMarketsToBuild, Double criterionValue, Double minBudget, Double companyValue, Double yearIncome, Double yearCost) {
        this.year = new SimpleIntegerProperty(year);
        this.smallMarketsToBuild = new SimpleIntegerProperty(smallMarketsToBuild);
        this.mediumMarketsToBuild = new SimpleIntegerProperty(mediumMarketsToBuild);
        this.bigMarketsToBuild = new SimpleIntegerProperty(bigMarketsToBuild);
        this.criterionValue = new SimpleDoubleProperty(criterionValue);
        this.minBudget = new SimpleDoubleProperty(minBudget);
        this.companyValue = new SimpleDoubleProperty(companyValue);
        this.yearIncome = new SimpleDoubleProperty(yearIncome);
        this.yearCost = new SimpleDoubleProperty(yearCost);
    }


    public int getYear() {
        return year.get();
    }

    public SimpleIntegerProperty yearProperty() {
        return year;
    }

    public int getSmallMarketsToBuild() {
        return smallMarketsToBuild.get();
    }

    public SimpleIntegerProperty smallMarketsToBuildProperty() {
        return smallMarketsToBuild;
    }

    public int getMediumMarketsToBuild() {
        return mediumMarketsToBuild.get();
    }

    public SimpleIntegerProperty mediumMarketsToBuildProperty() {
        return mediumMarketsToBuild;
    }

    public int getBigMarketsToBuild() {
        return bigMarketsToBuild.get();
    }

    public SimpleIntegerProperty bigMarketsToBuildProperty() {
        return bigMarketsToBuild;
    }

    public double getCriterionValue() {
        return criterionValue.get();
    }

    public SimpleDoubleProperty criterionValueProperty() {
        return criterionValue;
    }

    public double getMinBudget() {
        return minBudget.get();
    }

    public SimpleDoubleProperty minBudgetProperty() {
        return minBudget;
    }

    public double getCompanyValue() {
        return companyValue.get();
    }

    public SimpleDoubleProperty companyValueProperty() {
        return companyValue;
    }

    public double getYearIncome() {
        return yearIncome.get();
    }

    public SimpleDoubleProperty yearIncomeProperty() {
        return yearIncome;
    }

    public double getYearCost() {
        return yearCost.get();
    }

    public SimpleDoubleProperty yearCostProperty() {
        return yearCost;
    }

}
