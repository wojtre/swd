package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Wojciech Trefon on 14.05.2016.
 */
public class Main extends Application {

    private static final String PROGRAM_NAME = "Stonka’s Masters 2000 Enterprise Edition v.10 Professional";

    public static void main(String[] args) {
	launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
	Parent root = FXMLLoader.load(getClass().getResource("/client/mainWindow/mainWindow.fxml"));
	Scene scene = new Scene(root);

	stage.setTitle(PROGRAM_NAME);
	stage.setScene(scene);
	stage.show();
    }
}