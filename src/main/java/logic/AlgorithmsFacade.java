package logic;

import Jama.Matrix;
import client.ahp.Preferences;
import client.mainWindow.InputDataCTO;
import client.mainWindow.ResultTO;
import logic.dynamicprograming.AHP;
import logic.dynamicprograming.DynamicPrograming;
import logic.dynamicprograming.State;

import java.util.List;
import java.util.Optional;

/**
 * Created by Wojciech Trefon on 20.05.2016.
 */
public class AlgorithmsFacade {
    private AhpConverter ahpConverter = new AhpConverter();
    private DynamicProgrammingConverter dynamicProgrammingConverter = new DynamicProgrammingConverter();

    public ResultTO calculateResult(InputDataCTO to){
        DynamicPrograming dynamicPrograming = dynamicProgrammingConverter.convertInputDataTO(to);
        Optional<List<State>> states = dynamicPrograming.startAlgorithm();
        return dynamicProgrammingConverter.convertListOfStates(states.get());
    }

    public double[] calculateAhpProportions(Preferences preferences) {
        Matrix matrix = ahpConverter.convertPreferences(preferences);
        return AHP.calculatePreferenceVector(matrix);
    }

    public Preferences improveMatrixConsistency(Preferences preferences) {
        Matrix matrix = ahpConverter.convertPreferences(preferences);
        Matrix improvedMatrix = AHP.improveMatrixConsistencyHeuristicAlg(matrix);
        return ahpConverter.convertMatrixPreferences(improvedMatrix);
    }

    public boolean isMatrixConsistent(Preferences preferences) {
        Matrix matrix = ahpConverter.convertPreferences(preferences);
        return AHP.isMatrixConsistent(matrix);
    }
}
