package logic;

import Jama.Matrix;
import client.mainWindow.GeneralResult;
import client.mainWindow.InputDataCTO;
import client.mainWindow.ResultTO;
import client.mainWindow.StepResult;
import logic.dynamicprograming.AHPStrategy;
import logic.dynamicprograming.DynamicPrograming;
import logic.dynamicprograming.Parameters;
import logic.dynamicprograming.State;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Wojciech Trefon on 20.05.2016.
 */
public class DynamicProgrammingConverter {
    public DynamicPrograming convertInputDataTO(InputDataCTO data) {
        AHPStrategy ahpStrategy = new AHPStrategy(data.getDochodProportion(), data.getKosztyProportion(),
                data.getWartoscProportion());

        Parameters parameters = new Parameters();
        parameters.setIncome(data.getParametersTO().getIncomeInArray());
        parameters.setProgression(new double[]{0.8, 0.5});
        parameters.setCost(data.getParametersTO().getCostInArray());
        parameters.setBuildCost(data.getParametersTO().getPriceInArray());

        // parameters.printParameters();

        parameters.setIncome(new double[]{100, 200, 300});
        parameters.setProgression(new double[]{0.8, 0.5});
        parameters.setCost(new double[]{50, 80, 110});
        parameters.setBuildCost(new double[]{500, 800, 1100});

        DynamicPrograming dynamicPrograming = new DynamicPrograming(data.getYearsNr(), parameters);
        dynamicPrograming.setNumOfMarketsPerYear(data.getMarketsPerYear());
        dynamicPrograming.setAHPStrategy(ahpStrategy);
        dynamicPrograming.setEndState(data.getSmallNr(), data.getMediumNr(), data.getBigNr());
        dynamicPrograming.setBudget(data.getBudget());
        return dynamicPrograming;
    }

    public ResultTO convertListOfStates(List<State> states) {

        List<GeneralResult> generalResults = new LinkedList<>();

        for (State state : states) {
            List<State> finalStates = state.getAllParentStates();
            State finalState = finalStates.get(finalStates.size() - 1);
            GeneralResult result = new GeneralResult(state.getCriterionValue(), state.getCumulativeMinCashRequiedToBuildMarkets(),
                    finalState.getCompanyValue(), finalState.getIncome(), finalState.getCost());

            generalResults
                    .add(result);

            List<StepResult> parentResults = new LinkedList<>();
            for (State parentState : finalStates) {
                parentResults.add(this.convertState(parentState));
            }
            result.setDetailedResult(parentResults);
        }
        ResultTO resultTo = new ResultTO();
        resultTo.setGeneralResults(generalResults);

        return resultTo;
    }

    public StepResult convertState(State state) {
        Matrix marketsToBuild = state.getDecision().getDecisionVector();

        final int year = state.getYear();
        final int smallMarketsToBuild = (int) marketsToBuild.get(0, 0);
        final int mediumMarketsToBuild = (int) marketsToBuild.get(1, 0);
        final int bigMarketsToBuild = (int) marketsToBuild.get(2, 0);
        final double criterionValue = state.getCriterionValue();
        final double minBudget = state.getCumulativeMinCashRequiedToBuildMarkets();
        final double companyValue = state.getCompanyValue();
        final double yearIncome = state.getIncome();
        final double yearCost = state.getCost();

        return new StepResult(year, smallMarketsToBuild, mediumMarketsToBuild, bigMarketsToBuild, criterionValue,
                minBudget, companyValue, yearIncome, yearCost);
    }
}
