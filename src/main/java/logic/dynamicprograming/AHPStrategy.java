package logic.dynamicprograming;

/**
 * This class provides an algorithm for calculating criterion value based on
 * state. It is used as as Strategy in Strategy Design Pattern in State.
 * 
 * @author Adrian Wosiński
 */
public class AHPStrategy {

    double lambda1;
    double lambda2;
    double lambda3;

    /**
     * @param lambda1
     *            - income criterion parameter
     * @param lambda2
     *            - cost criterion parameter
     * @param lambda3
     *            - company value criterion parameter
     */
    public AHPStrategy(double lambda1, double lambda2, double lambda3) {
	this.lambda1 = lambda1;
	this.lambda2 = lambda2;
	this.lambda3 = lambda3;
    }

    /**
     * Calculates local criterion value for given state based on parameters
     * lambda (AHP ranking)
     * 
     * @param state
     *            - State instance for which criterion value is calculated
     * @return local criterion value
     */
    public double calculateCriterionValue(State state) {
	return lambda1 * state.getIncome() + lambda2 * state.getCost() + lambda3 * state.getCompanyValue();
    }

}
