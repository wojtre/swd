package logic.dynamicprograming;

/**
 * Possible time period for which last decisions are remembered in State
 * instance
 * 
 * @author Adrian Wosiński
 */
public enum LastPeriod {
    TWO_YEARS_AGO, ONE_YEAR_AGO
}
