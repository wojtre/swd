package logic.dynamicprograming;

import Jama.Matrix;

/**
 * Created by Arnold on 2016-05-19.
 */
public class AHP {
    //RI index for matrix of size 3
    private final static double RANDOM_INDEX = 0.58D;
    //maximum CR value
    private final static double CONSISTENCY_RATIO_MAX = 0.1D;

    //quantity of iterations to safe stop of heuristic algorithm
    private final static int STOP = 100000;
    //quantity of rows in matrix
    private final static int ROW_NUM = 3;
    //quantity of cols in matrix
    private final static int COL_NUM = 3;


    /**
     *  AHP count preference vector of given preference matrix.
     *
     * @param preferenceMatrix
     *          - matrix of double values from set = {1/9, 1/8, ..., 1/2, 1 , 2, ...,9 }
     * @return
     *          - null if matrix is not of size 3x3 or null
     *          - double[] of three values, the calculated preference vector that AHP counts, values used in AHPStrategy class
     */
    public static double[] calculatePreferenceVector(Matrix preferenceMatrix) {
        if (!checkMatrixDimension(preferenceMatrix)) {
            return null;
        }
        //Matrix normalization
        Matrix normalizedMatrix = AHP.normalizeMatrix(preferenceMatrix);

        //count preferenceVector
        return AHP.countPreferenceVector(normalizedMatrix);

    }

    /**
     *  checks whether given matrix is consistent
     *
     * @param preferenceMatrix
     *          - matrix of double values from set = {1/9, 1/8, ..., 1/2, 1 , 2, ...,9 }
     * @return true if given matrix is consistent (CR < 0.1 ), false otherwise (CR >= 0.1)
     */
    public static boolean isMatrixConsistent(Matrix preferenceMatrix) {
        double consistencyRatio = AHP.countMatrixConsistency(preferenceMatrix);
        return (consistencyRatio <= CONSISTENCY_RATIO_MAX);
    }


    //works only with matrices of size 3

    /**
     *  returns preference matrix that is consistent or null otherwise
     *
     * @param givenPreferenceMatrix
     *          - matrix of double values from set = {1/9, 1/8, ..., 1/2, 1 , 2, ...,9 }
     * @return
     *          - null when givenMatrix is not of size 3x3 or is null, also null when there is no such matrix
     *          - the same matrix given as parameter when the matrix is consistent
     *          - otherwise new preference Matrix of values { 1/9, 1/8, ..., 1/2, 1 , 2, ...,9 } is given
     */
    public static Matrix improveMatrixConsistencyHeuristicAlg(Matrix givenPreferenceMatrix) {
        if (!checkMatrixDimension(givenPreferenceMatrix)) {
            return null;
        }
        if (AHP.isMatrixConsistent(givenPreferenceMatrix)) {
            return givenPreferenceMatrix;
        }
        Matrix newPreferenceMatrix = (Matrix) givenPreferenceMatrix.clone();

        double crBeforeAdjustment = 1.0D;

        int iter = 0;
        while ((crBeforeAdjustment > CONSISTENCY_RATIO_MAX) && (iter < STOP)) {
            //find greatest deviation
            int[] indicators = AHP.findGreatestDeviation(newPreferenceMatrix);
            final int maxi = indicators[0];
            final int maxk = indicators[1];
            final int maxj = indicators[2];
            newPreferenceMatrix = AHP.matrixAdjustment(newPreferenceMatrix, maxi, maxk, maxj);
            crBeforeAdjustment = AHP.countMatrixConsistency(newPreferenceMatrix);
            iter++;
        }
        if (crBeforeAdjustment <= CONSISTENCY_RATIO_MAX) {
            return newPreferenceMatrix;
        }
        return null;
    }

    private static double[] sumColumnElements(Matrix preferenceMatrix) {

        double[] sums = new double[COL_NUM];

        for (int j = 0; j < COL_NUM; j++) {
            sums[j] = 0.0D;
            for (int i = 0; i < ROW_NUM; i++) {
                sums[j] += preferenceMatrix.get(i, j);
            }
        }
        return sums;
    }

    private static Matrix normalizeMatrix(Matrix preferenceMatrix) {

        //count column sums
        double[] c = sumColumnElements(preferenceMatrix);

        Matrix normalizedMatrix = new Matrix(ROW_NUM, COL_NUM);
        for (int j = 0; j < COL_NUM; j++) {
            for (int i = 0; i < ROW_NUM; i++) {
                double newValue = preferenceMatrix.get(i, j) / c[j];
                normalizedMatrix.set(i, j, newValue);
            }
        }
        return normalizedMatrix;
    }

    private static double[] countPreferenceVector(Matrix normalizedMatrix) {
        double[] preferenceVector = new double[ROW_NUM];

        for (int i = 0; i < ROW_NUM; i++) {
            preferenceVector[i] = 0.0D;
            for (int j = 0; j < COL_NUM; j++) {
                preferenceVector[i] += normalizedMatrix.get(i, j);
            }
            preferenceVector[i] /= COL_NUM;
        }
        return preferenceVector;
    }

    private static boolean checkMatrixDimension(Matrix preferenceMatrix) {
        if (preferenceMatrix != null) {
            if (preferenceMatrix.getRowDimension() == ROW_NUM && preferenceMatrix.getColumnDimension() == COL_NUM) {
                return true;
            }
        }
        return false;
    }

    private static double countMatrixConsistency(Matrix preferenceMatrix) {
        if (!checkMatrixDimension(preferenceMatrix)) {
            return 1.0;
        }
        //count Consistency Index, have to get preference Vector and
        //AHP ahp = new AHP(preferenceMatrix);
        double[] sums = AHP.sumColumnElements(preferenceMatrix);
        Matrix normalizedMatrix = AHP.normalizeMatrix(preferenceMatrix);
        double[] preferenceVector = AHP.countPreferenceVector(normalizedMatrix);

        double consistencyIndex = 0.0D;
        for (int i = 0; i < COL_NUM; i++) {
            consistencyIndex += sums[i] * preferenceVector[i];
        }
        consistencyIndex -= COL_NUM;

        return consistencyIndex / RANDOM_INDEX;
    }



    private static int[] findGreatestDeviation(Matrix preferenceMatrix) {
        int[] indicators = {0, 0, 0};
        double cgMax = 0.0;
        double cg;
        double correctValue;
        for (int i = 0; i < COL_NUM; i++) {
            for (int j = 0; j < COL_NUM; j++) {
                for (int k = 0; k < COL_NUM; k++) {
                    correctValue = preferenceMatrix.get(i, k) * preferenceMatrix.get(k, j);
                    cg = preferenceMatrix.get(i, j) - correctValue;
                    if (cg > cgMax && i != j) {
                        cgMax = cg;
                        indicators[0] = i;
                        indicators[1] = k;
                        indicators[2] = j;
                    }
                }
            }
        }
        return indicators;

    }

    private static Matrix matrixAdjustment(Matrix preferenceMatrix, final int maxi, final int maxk, final int maxj) {
        double crBeforeAdjustment = AHP.countMatrixConsistency(preferenceMatrix);
        Matrix testMatrix = (Matrix) preferenceMatrix.clone();
        double correctValue = preferenceMatrix.get(maxi, maxk) * preferenceMatrix.get(maxk, maxj);
        double cgRevise = preferenceMatrix.get(maxi, maxj) - correctValue;
        if (Double.compare(cgRevise, 0.0D) < 0) {
            double value = preferenceMatrix.get(maxi, maxj) + 2.0;
            testMatrix.set(maxi, maxj, value);
            testMatrix.set(maxj, maxi, 1.0D / value);
        }
        if (Double.compare(cgRevise, 0.0D) > 0) {
            double value = preferenceMatrix.get(maxi, maxj) - 2.0;
            testMatrix.set(maxi, maxj, value);
            testMatrix.set(maxj, maxi, 1.0D / value);
        }
        final double crAfterAdjustment = AHP.countMatrixConsistency(testMatrix);
        if (crAfterAdjustment < crBeforeAdjustment) {
            return testMatrix;
        }
        return preferenceMatrix;
    }


    /*
    public static void main(String[] args) {
        double[][] values = {{1.0, 3.0, 7.0}, {0.33, 1.0, 5.0}, {0.143, 0.2, 1.0}};
        Matrix matrix = new Matrix(values);
        //Matrix normMat = AHP.normalizeMatrix(matrix);

        for (int i = 0; i < ROW_NUM; i++) {
            for (int j = 0; j < COL_NUM; j++) {
                System.out.print(matrix.get(i, j) + "  ");
            }
            System.out.println();
        }

        Matrix newMatrix = AHP.improveMatrixConsistencyHeuristicAlg(matrix);
        System.out.println();
        //System.out.println(AHP.countMatrixConsistency(newMatrix));
        for (int i = 0; i < ROW_NUM; i++) {
            for (int j = 0; j < COL_NUM; j++) {
                System.out.print(newMatrix.get(i, j) + "  ");
            }
            System.out.println();
        }
    }
    */

}
