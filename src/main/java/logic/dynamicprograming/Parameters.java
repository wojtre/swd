package logic.dynamicprograming;

import Jama.Matrix;

/**
 * This class is created as a container of pareters of marketes and is used in
 * DynamicPrograming algorithm and in State for calculate income, cost and the
 * rest of State parameters
 * 
 * @author Adrian Wosiński
 */
public class Parameters {

    private Matrix income;
    private Matrix cost;
    private Matrix progression;
    private Matrix incomeProgression;
    private Matrix buildCost;

    public Matrix getIncome() {
	return income;
    }

    public void setIncome(double[] income) {
	this.income = new Matrix(new double[][] { income });
	calculateProgression();
    }

    public Matrix getCost() {
	return cost;
    }

    public void setCost(double[] cost) {
	this.cost = new Matrix(new double[][] { cost });
    }

    public Matrix getProgression() {
	return progression;
    }

    public void setProgression(double[] progression) {
	this.progression = new Matrix(new double[][] { progression });
	calculateProgression();
    }

    private void calculateProgression() {
	if (income != null && progression != null) {
	    incomeProgression = income.transpose().times(progression);
	}
    }

    public Matrix getIncomeProgression() {
	return incomeProgression;
    }

    public Matrix getBuildCost() {
	return buildCost;
    }

    public void setBuildCost(double[] buildCost) {
	this.buildCost = new Matrix(new double[][] { buildCost });
    }

    public void printParameters() {
	System.out.println("income");
	income.print(1, 0);
	System.out.println("cost");
	cost.print(1, 0);
	System.out.println("build cost");
	buildCost.print(1, 0);
    }
}
