package logic.dynamicprograming;

import Jama.Matrix;

/**
 * Represents decision as a number of markets that are builded in given step
 * 
 * @author Adrian Wosiński
 */
public class Decision {
    private Matrix decisionVector = new Matrix(new double[3][1]);
    private Parameters parameters;
    private double buildCost;

    private int year;

    public Decision(int smallMarkets, int mediumMarkets, int bigMarkets, int year, Parameters parameters) {
	this.parameters = parameters;
	decisionVector.set(MarketType.SMALL.ordinal(), 0, smallMarkets);
	decisionVector.set(MarketType.MEDIUM.ordinal(), 0, mediumMarkets);
	decisionVector.set(MarketType.BIG.ordinal(), 0, bigMarkets);
	buildCost = parameters.getBuildCost().times(decisionVector).get(0, 0);
	this.year = year;
    }

    public Matrix getDecisionVector() {
        return decisionVector.copy();
    }

    public void setDecision(MarketType type, int number) {
	decisionVector.set(type.ordinal(), 0, number);
	buildCost = parameters.getBuildCost().times(decisionVector).get(0, 0);
    }

    /**
     * returns cost of markets which this decision represents
     * 
     * @return cost of this decision
     */
    public double getBuildCost() {
	return buildCost;
    }

    public int getYear() {
	return year;
    }

    @Override
    public String toString() {
	return "[" + decisionVector.get(0, 0) + ", " + decisionVector.get(1, 0) + ", " + decisionVector.get(2, 0) + "]";
    }
}
