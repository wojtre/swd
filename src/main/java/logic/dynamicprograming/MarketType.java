package logic.dynamicprograming;

/**
 * Possible markets types
 * 
 * @author Adrian Wosiński
 */
public enum MarketType {
    SMALL, MEDIUM, BIG
}
