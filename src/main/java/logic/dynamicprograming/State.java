package logic.dynamicprograming;

import java.util.LinkedList;
import java.util.List;

import Jama.Matrix;

/**
 * Represents state as a two last decision and builded markets.
 * 
 * @author Adrian Wosiński
 */
public class State {

    private final static boolean DEBUG = true;
    private final static int ONE_YEAR = 1;
    private final static int TWO_YEAR_DELAY = 2;
    private final static Matrix THREE_ONES_MATRIX = new Matrix(new double[][] { { 1, 1, 1 } });
    private final static Matrix TWO_ONES_MATRIX = new Matrix(new double[][] { { 1 }, { 1 } });

    private State parentState;
    private List<State> childrenStates = new LinkedList<State>();

    private Matrix lastDecisions = new Matrix(new double[3][2]);
    private Matrix buildedMarkets = new Matrix(new double[3][1]);

    private StateGenerator stateGenerator;
    private AHPStrategy ahpStrategy;
    private Decision decision;

    private int year;
    private double budget;

    private double income;
    private double cost;

    private double companyValue;
    private double minCashRequiedToBuildMarkets;

    private double cumulativeIncome;
    private double cumulativeCost;
    private double cumulativeCompanyValue;
    private double cumulativeMinCashRequiedToBuildMarkets;

    private double criterionValue;
    private Parameters parameters;

    /**
     * This constructor is used for creating new root State.
     * 
     * @param stateGenerator
     *            - gives an algorithm for generating states based on this state
     * @param ahpStrategy
     *            - gives an algorithm for calculating criterion value
     * @param year
     *            - represents year of this state
     */
    public State(DynamicPrograming dynamicPrograming) {
	this.stateGenerator = dynamicPrograming.getStateGenerator();
	this.parameters = dynamicPrograming.getParameters();
	this.ahpStrategy = dynamicPrograming.getAhpStrategy();
	this.year = dynamicPrograming.getYears() + TWO_YEAR_DELAY;
    }

    /**
     * Used to creating states based on other state that is understood as a
     * parent state of this newly created state.
     * 
     * @param parentState
     *            - state based on which this state is created
     */
    public State(State parentState) {
	this.parentState = parentState;
	this.parameters = parentState.getParameters();
	this.stateGenerator = parentState.getStateGenerator();
	this.ahpStrategy = parentState.getAHPStrategy();
	this.year = parentState.getYear() - ONE_YEAR;
	this.budget = parentState.getBudget();
    }

    public StateGenerator getStateGenerator() {
	return stateGenerator;
    }

    public AHPStrategy getAHPStrategy() {
	return ahpStrategy;
    }

    public int getYear() {
	return year;
    }

    public void setYear(int year) {
	this.year = year;
    }

    public State getParentState() {
	return parentState;
    }

    public List<State> getChildrenStates() {
	return childrenStates;
    }

    public void setDecision(Decision decision) {
	this.decision = decision;
    }

    public Decision getDecision() {
	return decision;
    }

    public double getIncome() {
	return income;
    }

    public double getCost() {
	return cost;
    }

    public double getCompanyValue() {
	return companyValue;
    }

    public double getCumulativeIncome() {
	return cumulativeIncome;
    }

    public double getCumulativeCost() {
	return cumulativeCost;
    }

    public double getCumulativeCompanyValue() {
	return cumulativeCompanyValue;
    }

    public double getCriterionValue() {
	return criterionValue;
    }

    public double getCashRequiedToBuildMarkets() {
	return minCashRequiedToBuildMarkets;
    }

    public double getCumulativeMinCashRequiedToBuildMarkets() {
	return cumulativeMinCashRequiedToBuildMarkets;
    }

    public double getBudget() {
	return budget;
    }

    public void setBudget(double budget) {
	this.budget = budget;
    }

    public Parameters getParameters() {
	return parameters;
    }

    public void setParameters(Parameters parameters) {
	this.parameters = parameters;
    }

    /**
     * Generates all possibly child states of this state that can be created
     * based on this state and rules specified by year, budged and possibility
     * of build all markets until given time period.
     * 
     * @return retVal - list of generated child states
     */
    public List<State> generateStates() {
	return generateStates(new LinkedList<State>());
    }

    private List<State> generateStates(List<State> retVal) {

	if (isPossibleToBuildAllMarketsInGivenTime() == false) {
	    return retVal;
	}

	if (year > 0) {
	    childrenStates = stateGenerator.generatePossibleStates(this);
	    for (State state : childrenStates) {
		if (budget != 0) {
		    if (budget <= state.getCumulativeMinCashRequiedToBuildMarkets()) {
			return retVal;
		    }
		}

		if (year == 1) {
		    retVal.addAll(childrenStates);
		    return retVal;
		}
		retVal = state.generateStates(retVal);
	    }
	} else {
	    retVal.addAll(childrenStates);
	}
	return retVal;
    }

    /**
     * Gives all state that are above this state includes this state in state
     * hierarchy - all parent states and its parent states.
     * 
     * @return list of parent states
     */
    public List<State> getAllParentStates() {
	List<State> states = new LinkedList<>();
	getAllParentStates(states);
	return states;
    }

    private void getAllParentStates(List<State> states) {
	states.add(this);
	if (parentState != null && year < stateGenerator.getYears()) {
	    parentState.getAllParentStates(states);
	}
    }

    public void setBuildedTwoYearsAgoMarkets(MarketType type, int number) {
	buildedMarkets.set(type.ordinal(), 0, number);
    }

    public void setLastDecisions(LastPeriod perdiod, MarketType type, int number) {
	lastDecisions.set(type.ordinal(), perdiod.ordinal(), number);
    }

    public int getBuildedTwoYearsAgoMarkets(MarketType type) {
	return (int) buildedMarkets.get(type.ordinal(), 0);
    }

    public int getBuildedMarkets(MarketType type) {
	return (int) (buildedMarkets.get(type.ordinal(), 0)
		+ lastDecisions.get(type.ordinal(), LastPeriod.ONE_YEAR_AGO.ordinal())
		+ lastDecisions.get(type.ordinal(), LastPeriod.TWO_YEARS_AGO.ordinal()));
    }

    public int getLastBuildedMarkets(MarketType type, LastPeriod period) {
	return (int) lastDecisions.get(type.ordinal(), period.ordinal());
    }

    public Matrix getLastDecisions() {
	return lastDecisions;
    }

    public Matrix getBuildedMarkets() {
	return buildedMarkets;
    }

    public void calculateAll() {
	calculateIncome();
	calculateCosts();
	calculateCompanyValue();
	calculateCriterionValue();
	calculateCashRequiedToMakeDecision();
    }

    public boolean isEmpty() {
	for (int i = 0; i < 3; i++) {
	    if (lastDecisions.get(i, 0) != 0 || lastDecisions.get(i, 1) != 0 || buildedMarkets.get(i, 0) != 0)
		return false;
	}
	return true;
    }

    public boolean isPossibleToBuildAllMarketsInGivenTime() {
	int sumOfBuildedMarkets = (int) THREE_ONES_MATRIX.times(buildedMarkets).get(0, 0);
	int numberOfMarketsThatCanToBeBuilded = stateGenerator.getMaxMarketsNumberPerYear() * (year - ONE_YEAR);

	if (sumOfBuildedMarkets > numberOfMarketsThatCanToBeBuilded) {
	    return false;
	}
	return true;
    }

    private void calculateIncome() {
	double incomeX2 = THREE_ONES_MATRIX.times(parameters.getIncomeProgression().arrayTimes(lastDecisions))
		.times(TWO_ONES_MATRIX).get(0, 0);

	double incomeX3 = parameters.getIncome().times(buildedMarkets).get(0, 0);

	income = incomeX2 + incomeX3;

	if (parentState != null)
	    cumulativeIncome = parentState.getCumulativeIncome() + income;
	else
	    cumulativeIncome = income;
    }

    private void calculateCosts() {
	double costX2 = parameters.getCost().times(lastDecisions.times(TWO_ONES_MATRIX)).get(0, 0);

	double costX3 = parameters.getCost().times(buildedMarkets).get(0, 0);

	cost = costX2 + costX3;

	if (parentState != null)
	    cumulativeCost = parentState.getCumulativeCost() + cost;
	else
	    cumulativeCost = cost;
    }

    private void calculateCompanyValue() {
	double companyValueX2 = parameters.getBuildCost().times(lastDecisions.times(TWO_ONES_MATRIX)).get(0, 0);

	double companyValueX3 = parameters.getBuildCost().times(buildedMarkets).get(0, 0);

	companyValue = companyValueX2 + companyValueX3;

	if (parentState != null)
	    cumulativeCompanyValue = parentState.getCumulativeCompanyValue() + companyValue;
	else
	    cumulativeCompanyValue = companyValue;
    }

    private void calculateCriterionValue() {
	if (parentState != null)
	    criterionValue = parentState.getCriterionValue() + ahpStrategy.calculateCriterionValue(this);
	else
	    criterionValue = ahpStrategy.calculateCriterionValue(this);
    }

    private void calculateCashRequiedToMakeDecision() {
	if (decision != null)
	    minCashRequiedToBuildMarkets = Math.max(0, decision.getBuildCost() + cost - income);
	if (parentState != null) {
	    cumulativeMinCashRequiedToBuildMarkets += parentState.getCumulativeMinCashRequiedToBuildMarkets()
		    + minCashRequiedToBuildMarkets;
	}
    }

    public void printState() {
	if (DEBUG) {
	    System.out.println("======================================");
	    System.out.println("year: " + getYear());
	    System.out.println("X_3");
	    getBuildedMarkets().print(4, 0);
	    System.out.println("X_2");
	    getLastDecisions().print(4, 0);
	    System.out.println("Income " + getIncome() + " zl");
	    System.out.println("Cumulative income " + getCumulativeIncome() + " zl");
	    System.out.println("Cost " + cost + " zl");
	    System.out.println("Cumulative cost " + getCumulativeCost() + " zl");
	    System.out.println("Criterion value " + criterionValue);
	    System.out.println(decision);
	    System.out.println("requied Cash: " + minCashRequiedToBuildMarkets);
	    System.out.println("requied Cash ALL: " + cumulativeMinCashRequiedToBuildMarkets);
	}
    }

    public void printParentStates() {
	printState();
	if (parentState != null) {
	    parentState.printParentStates();
	}
    }
}
