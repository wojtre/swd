package logic.dynamicprograming;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Adrian Wosiński
 */
public class DynamicPrograming {

    private final static int DEFAULT_NUM_OF_MARKETS_PER_YEAR = 1;
    private final static int YEAR_DELAY = 2;

    private int smallMarkets;
    private int mediumMarkets;
    private int bigMarkets;

    private int years;
    private double budget;
    private int numOfMarketsPerYear = DEFAULT_NUM_OF_MARKETS_PER_YEAR;

    private AHPStrategy ahpStrategy;
    private StateGenerator stateGenerator;
    private Parameters parameters;

    public DynamicPrograming(int years, Parameters parameters) {
	this.years = years + YEAR_DELAY;
	this.parameters = parameters;
	stateGenerator = new StateGenerator(years);
    }

    public int getYears() {
	return years - YEAR_DELAY;
    }

    public void setYears(int years) {
	this.years = years + YEAR_DELAY;
	stateGenerator.setYears(years);
    }

    public double getBudget() {
	return budget;
    }

    public void setBudget(double budget) {
	this.budget = budget;
    }

    public int getNumOfMarketsPerYear() {
	return numOfMarketsPerYear;
    }

    public void setNumOfMarketsPerYear(int numOfMarketsPerYear) {
	this.numOfMarketsPerYear = numOfMarketsPerYear;
	stateGenerator.setNumberOfMarketsPerYear(numOfMarketsPerYear);
    }

    public void setAHPStrategy(AHPStrategy ahpStrategy) {
	this.ahpStrategy = ahpStrategy;
    }

    public void setEndState(int smallMarkets, int mediumMarkets, int bigMarkets) {
	this.smallMarkets = smallMarkets;
	this.mediumMarkets = mediumMarkets;
	this.bigMarkets = bigMarkets;
    }

    /**
     * Starts dynamic programming algorithm and returns Optional of null if the
     * algorithm is not prepared or list of endStates (with year 0) if it is.
     * 
     * @return Optional of endStates List
     */
    public Optional<List<State>> startAlgorithm() {

	if (isAlgorithmPrepared() == false) {
	    System.err.println("Algorithm is not prepared");
	    return Optional.empty();
	}

	State state = buildFirstState();

	List<State> endStates = state.generateStates();

	endStates = filterNotEmptyStates(endStates);

	return Optional.of(endStates);
    }

    private List<State> filterNotEmptyStates(List<State> endStates) {
	return endStates.stream().filter(s -> s.isEmpty()).collect(Collectors.toList());
    }

    private State buildFirstState() {
	State state = new State(this);
	state.setBuildedTwoYearsAgoMarkets(MarketType.SMALL, smallMarkets);
	state.setBuildedTwoYearsAgoMarkets(MarketType.MEDIUM, mediumMarkets);
	state.setBuildedTwoYearsAgoMarkets(MarketType.BIG, bigMarkets);
	state.setBudget(budget);
	return state;
    }

    private boolean isAlgorithmPrepared() {
	if (ahpStrategy == null) {
	    return false;
	}
	return isStateOK();
    }

    private boolean isStateOK() {
	if (smallMarkets == 0 && mediumMarkets == 0 && bigMarkets == 0) {
	    return false;
	}
	return isYearOK();
    }

    private boolean isYearOK() {
	if (years < 1) {
	    return false;
	}
	return true;
    }

    public static void main(String[] args) {

	int maxNumOfMarketsPerYear = 1;
	int years = 10;

	double lambda1 = 0.3;
	double lambda2 = 0.3;
	double lambda3 = 0.3;

	int smallMarkets = 1;
	int mediumMarkets = 0;
	int bigMarkets = 0;

	AHPStrategy ahpStrategy = new AHPStrategy(lambda1, lambda2, lambda3);

	Parameters parameters = new Parameters();
	parameters.setIncome(new double[] { 100, 200, 300 });
	parameters.setProgression(new double[] { 0.8, 0.5 });
	parameters.setCost(new double[] { 50, 80, 110 });
	parameters.setBuildCost(new double[] { 500, 800, 1100 });

	DynamicPrograming dynamicPrograming = new DynamicPrograming(years, parameters);
	dynamicPrograming.setNumOfMarketsPerYear(maxNumOfMarketsPerYear);
	dynamicPrograming.setAHPStrategy(ahpStrategy);
	dynamicPrograming.setEndState(smallMarkets, mediumMarkets, bigMarkets);

	List<State> zeroState = dynamicPrograming.startAlgorithm().get();

	for (State state : zeroState) {
	    List<State> branch = state.getAllParentStates();
	    System.out.println("\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
	    System.out.println("NEXT BRANCH");
	    for (State branchState : branch) {
		branchState.printState();
	    }
	}

    }

    public Parameters getParameters() {
	return parameters;
    }

    public void setParameters(Parameters parameters) {
	this.parameters = parameters;
    }

    public AHPStrategy getAhpStrategy() {
	return ahpStrategy;
    }

    public StateGenerator getStateGenerator() {
	return stateGenerator;
    }
}
