package logic.dynamicprograming;

import java.util.LinkedList;
import java.util.List;

import Jama.Matrix;

/**
 * @author Adrian Wosiński
 */
public class StateGenerator {

    private int maxMarketsNumberPerYear;
    private int years;

    public StateGenerator(int years) {
	this.years = years;
    }

    public int getYears() {
	return years;
    }

    public void setYears(int years) {
	this.years = years;
    }

    public int getMaxMarketsNumberPerYear() {
	return maxMarketsNumberPerYear;
    }

    public void setNumberOfMarketsPerYear(int numOfMarketsPerYear) {
	this.maxMarketsNumberPerYear = numOfMarketsPerYear;
    }

    public List<State> generatePossibleStates(State parentState) {

	Matrix builded = parentState.getBuildedMarkets().copy();

	List<State> possibleStates = new LinkedList<State>();

	int smallMarketNumber = (int) builded.get(MarketType.SMALL.ordinal(), 0);
	int mediumMarketNumber = (int) builded.get(MarketType.MEDIUM.ordinal(), 0);
	int bigMarketNumber = (int) builded.get(MarketType.BIG.ordinal(), 0);

	Decision decision = prepareDecision(parentState);

	for (int i = 0; i <= smallMarketNumber && i <= maxMarketsNumberPerYear; i++) {

	    for (int j = 0; j <= mediumMarketNumber && i + j <= maxMarketsNumberPerYear; j++) {

		for (int k = 0; k <= bigMarketNumber && i + j + k <= maxMarketsNumberPerYear; k++) {

		    State stateTemp = new State(parentState);

		    uptadeLastDecisions(parentState, i, j, k, stateTemp);
		    updateBuildMarkets(parentState, i, j, k, stateTemp);
		    stateTemp.setDecision(decision);
		    if (stateTemp.getYear() <= years)
			stateTemp.calculateAll();
		    possibleStates.add(stateTemp);
		}
	    }
	}

	return possibleStates;
    }

    private Decision prepareDecision(State parentState) {
	return new Decision(parentState.getLastBuildedMarkets(MarketType.SMALL, LastPeriod.ONE_YEAR_AGO),
		parentState.getLastBuildedMarkets(MarketType.MEDIUM, LastPeriod.ONE_YEAR_AGO),
		parentState.getLastBuildedMarkets(MarketType.BIG, LastPeriod.ONE_YEAR_AGO), parentState.getYear() - 1,
		parentState.getParameters());
    }

    private void updateBuildMarkets(State state, int smallMarkets, int mediumMarkets, int bigMarkets, State stateTemp) {
	stateTemp.setBuildedTwoYearsAgoMarkets(MarketType.SMALL,
		state.getBuildedTwoYearsAgoMarkets(MarketType.SMALL) - smallMarkets);
	stateTemp.setBuildedTwoYearsAgoMarkets(MarketType.MEDIUM,
		state.getBuildedTwoYearsAgoMarkets(MarketType.MEDIUM) - mediumMarkets);
	stateTemp.setBuildedTwoYearsAgoMarkets(MarketType.BIG,
		state.getBuildedTwoYearsAgoMarkets(MarketType.BIG) - bigMarkets);
    }

    private void uptadeLastDecisions(State parentState, int smallMarkets, int mediumMarkets, int bigMarkets,
	    State newState) {
	uptadeOneYearsAgoDecision(parentState, newState);
	uptadeTwoYearAgoDecision(smallMarkets, mediumMarkets, bigMarkets, newState);
    }

    private void uptadeTwoYearAgoDecision(int smallMarkets, int mediumMarkets, int bigMarkets, State newState) {
	newState.setLastDecisions(LastPeriod.TWO_YEARS_AGO, MarketType.SMALL, smallMarkets);
	newState.setLastDecisions(LastPeriod.TWO_YEARS_AGO, MarketType.MEDIUM, mediumMarkets);
	newState.setLastDecisions(LastPeriod.TWO_YEARS_AGO, MarketType.BIG, bigMarkets);
    }

    private void uptadeOneYearsAgoDecision(State state, State stateTemp) {
	stateTemp.setLastDecisions(LastPeriod.ONE_YEAR_AGO, MarketType.SMALL,
		state.getLastBuildedMarkets(MarketType.SMALL, LastPeriod.TWO_YEARS_AGO));
	stateTemp.setLastDecisions(LastPeriod.ONE_YEAR_AGO, MarketType.MEDIUM,
		state.getLastBuildedMarkets(MarketType.MEDIUM, LastPeriod.TWO_YEARS_AGO));
	stateTemp.setLastDecisions(LastPeriod.ONE_YEAR_AGO, MarketType.BIG,
		state.getLastBuildedMarkets(MarketType.BIG, LastPeriod.TWO_YEARS_AGO));
    }
}
