package logic;

import static java.lang.Math.round;

import Jama.Matrix;
import client.ahp.Preferences;
import javafx.util.Pair;

/**
 * Created by Wojciech Trefon on 20.05.2016.
 */
public class AhpConverter {
    public Matrix convertPreferences(Preferences preferences) {
	double[][] values = { getFirstRow(preferences), getSecondRow(preferences), getThirdRow(preferences) };

	// debug matrix
	(new Matrix(values)).print(0, 2);

	return new Matrix(values);
    }

    public Preferences convertMatrixPreferences(Matrix matrixOfPreferences) {
	if (matrixOfPreferences == null) {
	    return null;
	}
	Preferences preferences = new Preferences();
	preferences.setDochodToKoszty(retrieveDochodToKoszty(matrixOfPreferences));
	preferences.setDochodToWartosc(retrieveDochodToWartosc(matrixOfPreferences));
	preferences.setKosztyToWartosc(retrieveKosztyToWartosc(matrixOfPreferences));
	return preferences;
    }

    private Pair<Integer, Integer> retrieveDochodToKoszty(Matrix matrixOfPreferences) {
	Pair<Integer, Integer> dochodToKoszty;
	if (isDochodGreaterThanKoszt(matrixOfPreferences)) {
	    dochodToKoszty = new Pair<>((int) round(matrixOfPreferences.get(0, 1)), 1);
	} else {
	    dochodToKoszty = new Pair<>(1, (int) round(matrixOfPreferences.get(1, 0)));
	}
	return dochodToKoszty;
    }

    private Pair<Integer, Integer> retrieveDochodToWartosc(Matrix matrixOfPreferences) {
	Pair<Integer, Integer> dochodToWartosc;
	if (isDochodGreaterThanWartosc(matrixOfPreferences)) {
	    dochodToWartosc = new Pair<>((int) round(matrixOfPreferences.get(0, 2)), 1);
	} else {
	    dochodToWartosc = new Pair<>(1, (int) round(matrixOfPreferences.get(2, 0)));
	}
	return dochodToWartosc;
    }

    private Pair<Integer, Integer> retrieveKosztyToWartosc(Matrix matrixOfPreferences) {
	Pair<Integer, Integer> kosztyToWartosc;
	if (isKosztGreaterThanWartosc(matrixOfPreferences)) {
	    kosztyToWartosc = new Pair<>((int) round(matrixOfPreferences.get(1, 2)), 1);
	} else {
	    kosztyToWartosc = new Pair<>(1, (int) round(matrixOfPreferences.get(2, 1)));
	}
	return kosztyToWartosc;
    }

    private boolean isDochodGreaterThanKoszt(Matrix matrixOfPreferences) {
	return matrixOfPreferences.get(0, 1) > matrixOfPreferences.get(1, 0);
    }

    private boolean isDochodGreaterThanWartosc(Matrix matrixOfPreferences) {
	return matrixOfPreferences.get(0, 2) > matrixOfPreferences.get(2, 0);
    }

    private boolean isKosztGreaterThanWartosc(Matrix matrixOfPreferences) {
	return matrixOfPreferences.get(1, 2) > matrixOfPreferences.get(2, 1);
    }

    private double[] getThirdRow(Preferences preferences) {
	return new double[] { preferences.getWartoscToDochodProportion(), preferences.getWartoscToKosztyProportion(),
		1 };
    }

    private double[] getSecondRow(Preferences preferences) {
	return new double[] { preferences.getKosztyToDochodProportion(), 1,
		preferences.getKosztyToWartoscProportion() };
    }

    private double[] getFirstRow(Preferences preferences) {
	return new double[] { 1, preferences.getDochodToKosztyProportion(),
		preferences.getDochodToWartoscProportion() };
    }
}
